<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>test</fullName>
        <apiVersion>40.0</apiVersion>
        <endpointUrl>https://trudelnudel.de</endpointUrl>
        <fields>AnnualRevenue</fields>
        <fields>Fax</fields>
        <fields>Id</fields>
        <fields>ParentId</fields>
        <fields>ShippingCity</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>tobi@parx.com</integrationUser>
        <name>test</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
</Workflow>
