({
    clickCreateItem: function(component, event, helper) {

        // Simplistic error checking
        var validItem = true;

        // Name must not be blank
        var nameField = component.find("itemname");
        var item = nameField.get("v.value");

        if ( $A.util.isEmpty( item ) ) {
            validItem = false;
            nameField.set( "v.errors", [{message:"Item name can't be blank."}]);
        } else {
            nameField.set( "v.errors", null );
        }

        nameField = component.find("quantity");
		item = nameField.get("v.value");
		if ( $A.util.isEmpty( item ) ) {
            validItem = false;
            nameField.set("v.errors", [{message:"Item quantity can't be blank."}]);
        } else {
            nameField.set("v.errors", null);
        }

		nameField = component.find("price");
		item = nameField.get("v.value");
		if ( $A.util.isEmpty( item ) ) {
            validItem = false;
            nameField.set("v.errors", [{message:"Item price can't be blank."}]);
        } else {
            nameField.set("v.errors", null);
        }

        // ... hint: more error checking here ...
        // If we pass error checking, do some real work
        if(validItem) {
            
            var newItem = component.get("v.newItem");
            console.log("New Item:" + newItem);

            // Create the new expense
            var newItems = component.get("v.items");
            newItems.push(JSON.parse(JSON.stringify(newItem)));
            
            component.set("v.items", newItems);
            component.set("v.newItem",{ 'sobjectType': 'Camping_Item__c','Name': '','Quantity__c': 0,
                    'Price__c': 0,'Packed__c': false });
            // helper.createItem(component, newItem);
        }
    }
})