public with sharing class StringArrayTest {
	public StringArrayTest() {
		
	}

	public static List<String>  generateStringArray(Integer size) {
		List<String> generatedList = new List<String>(); 
		for (Integer i=0; i < size; i++) {
			generatedList.add('Test ' + i);
		}
		return generatedList;
	}
}