<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>X1hour_warning_before_milestone_expires</fullName>
        <description>1hour warning before milestone expires</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>testemail/TestA</template>
    </alerts>
    <fieldUpdates>
        <fullName>CaseOwner</fullName>
        <field>OwnerId</field>
        <lookupValue>tobias.burckhardt@googlemail.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>CaseOwner</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
</Workflow>
