/**
	SubscribedAreaTrigger registering all triggerhandlers.

 	@author tbu
 	@copyright PARX
*/
trigger SubscribedAreaTrigger on Subscribed_Area__c (before insert)
{

	new Triggers()
			.bind(Triggers.Evt.beforeinsert, new SubscribedAreaTriggerHandler.DuplicateRecordsPreventer())
			.manage();

}