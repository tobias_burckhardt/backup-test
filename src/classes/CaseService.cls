global class CaseService {
    global static void closeCases(Set<ID> caseIds, String reason) {
       List<Case> cases =
            [select status from Case where Id in :caseIds];
  
        	for(Case caseItem : cases) {
            	caseItem.status='closed';
            }
    }
}