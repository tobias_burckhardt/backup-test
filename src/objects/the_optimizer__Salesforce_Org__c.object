<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>The Salesforce Org object allows you to track which features are enabled on one or more orgs.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>true</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>MetadataBackupEnabled__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <inlineHelpText>If enabled, it may take up to 24 hours for the Metadata Backup to be performed.</inlineHelpText>
        <label>Metadata Backup Enabled</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>MetadataBackupLastRun__c</fullName>
        <externalId>false</externalId>
        <label>Metadata Backup Last Run</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>MetadataBackupOutput__c</fullName>
        <externalId>false</externalId>
        <label>Metadata Backup Output</label>
        <length>32768</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>MetadataBackupSuccess__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Metadata Backup Success</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>MetadataRepository__c</fullName>
        <description>HTTPS Git clone URL of the repository</description>
        <externalId>false</externalId>
        <inlineHelpText>Example: https://bitbucket.org/sikaglobal/global.git</inlineHelpText>
        <label>Metadata Repository</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Url</type>
    </fields>
    <fields>
        <fullName>Password__c</fullName>
        <description>Password for default admin user</description>
        <externalId>false</externalId>
        <inlineHelpText>Password for default admin user</inlineHelpText>
        <label>Password</label>
        <length>175</length>
        <maskChar>asterisk</maskChar>
        <maskType>all</maskType>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>EncryptedText</type>
    </fields>
    <fields>
        <fullName>SecurityToken__c</fullName>
        <description>Security Token for default admin user</description>
        <externalId>false</externalId>
        <inlineHelpText>Security Token for default admin user</inlineHelpText>
        <label>Security Token</label>
        <length>175</length>
        <maskChar>asterisk</maskChar>
        <maskType>all</maskType>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>EncryptedText</type>
    </fields>
    <fields>
        <fullName>Username__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>Username for default admin user</description>
        <externalId>true</externalId>
        <inlineHelpText>Username for default admin user</inlineHelpText>
        <label>Username</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>the_optimizer__Chatter_Enabled__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Chatter Enabled?</inlineHelpText>
        <label>Chatter Enabled?</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>the_optimizer__Instance_Location__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Instance Location</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>North America</fullName>
                    <default>false</default>
                    <label>North America</label>
                </value>
                <value>
                    <fullName>EMEA</fullName>
                    <default>false</default>
                    <label>EMEA</label>
                </value>
                <value>
                    <fullName>APJ</fullName>
                    <default>false</default>
                    <label>APJ</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>the_optimizer__Instance__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Instance</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>AP0</fullName>
                    <default>false</default>
                    <label>AP0</label>
                </value>
                <value>
                    <fullName>AP1</fullName>
                    <default>false</default>
                    <label>AP1</label>
                </value>
                <value>
                    <fullName>AP2</fullName>
                    <default>false</default>
                    <label>AP2</label>
                </value>
                <value>
                    <fullName>EU0</fullName>
                    <default>false</default>
                    <label>EU0</label>
                </value>
                <value>
                    <fullName>EU1</fullName>
                    <default>false</default>
                    <label>EU1</label>
                </value>
                <value>
                    <fullName>EU2</fullName>
                    <default>false</default>
                    <label>EU2</label>
                </value>
                <value>
                    <fullName>EU3</fullName>
                    <default>false</default>
                    <label>EU3</label>
                </value>
                <value>
                    <fullName>EU5</fullName>
                    <default>false</default>
                    <label>EU5</label>
                </value>
                <value>
                    <fullName>EU6</fullName>
                    <default>false</default>
                    <label>EU6</label>
                </value>
                <value>
                    <fullName>NA1</fullName>
                    <default>false</default>
                    <label>NA1</label>
                </value>
                <value>
                    <fullName>NA10</fullName>
                    <default>false</default>
                    <label>NA10</label>
                </value>
                <value>
                    <fullName>NA11</fullName>
                    <default>false</default>
                    <label>NA11</label>
                </value>
                <value>
                    <fullName>NA12</fullName>
                    <default>false</default>
                    <label>NA12</label>
                </value>
                <value>
                    <fullName>NA13</fullName>
                    <default>false</default>
                    <label>NA13</label>
                </value>
                <value>
                    <fullName>NA14</fullName>
                    <default>false</default>
                    <label>NA14</label>
                </value>
                <value>
                    <fullName>NA15</fullName>
                    <default>false</default>
                    <label>NA15</label>
                </value>
                <value>
                    <fullName>NA16</fullName>
                    <default>false</default>
                    <label>NA16</label>
                </value>
                <value>
                    <fullName>NA17</fullName>
                    <default>false</default>
                    <label>NA17</label>
                </value>
                <value>
                    <fullName>NA18</fullName>
                    <default>false</default>
                    <label>NA18</label>
                </value>
                <value>
                    <fullName>NA19</fullName>
                    <default>false</default>
                    <label>NA19</label>
                </value>
                <value>
                    <fullName>NA2</fullName>
                    <default>false</default>
                    <label>NA2</label>
                </value>
                <value>
                    <fullName>NA20</fullName>
                    <default>false</default>
                    <label>NA20</label>
                </value>
                <value>
                    <fullName>NA21</fullName>
                    <default>false</default>
                    <label>NA21</label>
                </value>
                <value>
                    <fullName>NA22</fullName>
                    <default>false</default>
                    <label>NA22</label>
                </value>
                <value>
                    <fullName>NA23</fullName>
                    <default>false</default>
                    <label>NA23</label>
                </value>
                <value>
                    <fullName>NA24</fullName>
                    <default>false</default>
                    <label>NA24</label>
                </value>
                <value>
                    <fullName>NA26</fullName>
                    <default>false</default>
                    <label>NA26</label>
                </value>
                <value>
                    <fullName>NA3</fullName>
                    <default>false</default>
                    <label>NA3</label>
                </value>
                <value>
                    <fullName>NA4</fullName>
                    <default>false</default>
                    <label>NA4</label>
                </value>
                <value>
                    <fullName>NA41</fullName>
                    <default>false</default>
                    <label>NA41</label>
                </value>
                <value>
                    <fullName>NA5</fullName>
                    <default>false</default>
                    <label>NA5</label>
                </value>
                <value>
                    <fullName>NA6</fullName>
                    <default>false</default>
                    <label>NA6</label>
                </value>
                <value>
                    <fullName>NA7</fullName>
                    <default>false</default>
                    <label>NA7</label>
                </value>
                <value>
                    <fullName>NA8</fullName>
                    <default>false</default>
                    <label>NA8</label>
                </value>
                <value>
                    <fullName>NA9</fullName>
                    <default>false</default>
                    <label>NA9</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>the_optimizer__Multi_Currency_Enabled__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Multi-Currency Enabled?</inlineHelpText>
        <label>Multi-Currency Enabled?</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>the_optimizer__Multi_Language__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Multi-Language?</inlineHelpText>
        <label>Multi-Language?</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>the_optimizer__No_FeaturesOnRoadmap__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>No Features On Roadmap</label>
        <summaryFilterItems>
            <field>the_optimizer__Org2Feature__c.the_optimizer__Status__c</field>
            <operation>equals</operation>
            <value>4. Review Complete - Place on Roadmap</value>
        </summaryFilterItems>
        <summaryForeignKey>the_optimizer__Org2Feature__c.the_optimizer__Org__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>the_optimizer__No_FeaturesScheduled__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>No Features Scheduled</label>
        <summaryFilterItems>
            <field>the_optimizer__Org2Feature__c.the_optimizer__Status__c</field>
            <operation>equals</operation>
            <value>5. Review Complete - Schedule for Release</value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>the_optimizer__Org2Feature__c.the_optimizer__Roadmap__c</field>
            <operation>equals</operation>
            <value>Small Enhancement Release, Short Term Roadmap, Long Term Roadmap</value>
        </summaryFilterItems>
        <summaryForeignKey>the_optimizer__Org2Feature__c.the_optimizer__Org__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>the_optimizer__No_Features_Assigned__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>No Features Assigned</label>
        <summaryForeignKey>the_optimizer__Org2Feature__c.the_optimizer__Org__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>the_optimizer__No_Features_Deployed__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>No Features Deployed</label>
        <summaryFilterItems>
            <field>the_optimizer__Org2Feature__c.the_optimizer__Status__c</field>
            <operation>equals</operation>
            <value>6. Deployed into Production</value>
        </summaryFilterItems>
        <summaryForeignKey>the_optimizer__Org2Feature__c.the_optimizer__Org__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>the_optimizer__Ord_ID__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Ord ID</label>
        <length>40</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>the_optimizer__SSO_Enabled__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>SSO Enabled?</inlineHelpText>
        <label>SSO Enabled?</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Salesforce Org</label>
    <listViews>
        <fullName>Metadata_Backup</fullName>
        <columns>NAME</columns>
        <columns>Username__c</columns>
        <columns>MetadataBackupEnabled__c</columns>
        <columns>MetadataBackupSuccess__c</columns>
        <columns>MetadataBackupLastRun__c</columns>
        <filterScope>Everything</filterScope>
        <label>Metadata Backup</label>
    </listViews>
    <listViews>
        <fullName>the_optimizer__All</fullName>
        <columns>NAME</columns>
        <columns>the_optimizer__Instance__c</columns>
        <columns>the_optimizer__Instance_Location__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Salesforce Org Name</label>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Salesforce Orgs</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>the_optimizer__Instance__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>the_optimizer__Instance_Location__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>the_optimizer__Ord_ID__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>the_optimizer__Instance__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>the_optimizer__Instance_Location__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>the_optimizer__Ord_ID__c</lookupDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>the_optimizer__Instance__c</searchFilterFields>
        <searchFilterFields>the_optimizer__Instance_Location__c</searchFilterFields>
        <searchFilterFields>the_optimizer__Ord_ID__c</searchFilterFields>
        <searchResultsAdditionalFields>the_optimizer__Instance__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>the_optimizer__Instance_Location__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
