public with sharing class AccountHandler 
{
	public static Account insertNewAccount(String name) 
	{
		Account account = new Account(Name=name);
		
		try 
		{
			insert account;
		} 
		catch (DmlException dmlEx) 
		{
			return null;
		}

		return account;
	}

	
}