/**
    <description>
     
    @author tobias
    @copyright PARX   
*/
/**
 *  TriggerTemplate version 2.0
 *
 *  This TriggerTemplate prevents triggers from being chained and has to be used
 *  for all apex triggers. For a description on its usage see TriggerTemplate.md
 *
 *  @copyright PARX
 *  All rights reserved.
 */
public with sharing class TriggerHandlerV2Debug
{
	// Flag to disable all trigger handlers. Should be used in visualforce controllers
	public static Boolean allTriggersDisabled = false;

	// Two lists to keep a record of the invoced trigger handlers, for debugging etc.
	public static List<String> beforeHandlerInvocations = new List<String>();
	public static List<String> afterHandlerInvocations = new List<String>();

	// Flag to enable all triggers. Should only be used when setting up an unit test
	private static Boolean allTriggersEnabled = false;

	// Names of enabled trigger handler classes
	private static String enabledBeforeHandlerClassName = null;
	private static String enabledAfterHandlerClassName = null;

	// Trigger event name (for example: insert, undelete etc)
	private static String enabledTriggerEventName = null;
	private static String contextID;

	// Names of Trigger event
	private static final String INSERT_EVENT_NAME = 'insert';
	private static final String UPDATE_EVENT_NAME = 'update';
	private static final String DELETE_EVENT_NAME = 'delete';
	private static final String UNDELETE_EVENT_NAME = 'undelete';

	// Flag to initialize trigger handler classes
	private static Boolean isInitialized = false;


    /**
        Mandatory when setting up an unit test: enable all triggers
    */
	public static void setupUnitTest()
	{
		allTriggersEnabled = true;

	}

    /**
        Mandatory when start unit test: disable all triggers
    */
	public static void startUnitTest()
	{
		isInitialized = false;
		allTriggersEnabled = false;
	}


	public class TriggerManager
	{
		private BeforeTriggerHandler beforeHandler;
		private AfterTriggerHandler afterHandler;

		private String beforeHandlerClassName;
		private String afterHandlerClassName;

		public TriggerManager(TriggerHandlerAdapter triggerHandlerAdapter)
		{
			this( 'UNKNOWN', triggerHandlerAdapter, triggerHandlerAdapter);
		}

		public TriggerManager(String theContextID, TriggerHandlerAdapter triggerHandlerAdapter)
		{
			this( theContextID, triggerHandlerAdapter, triggerHandlerAdapter );
		}

		public TriggerManager(String theContextID, BeforeTriggerHandler theBeforeHandler, AfterTriggerHandler theAfterHandler)
		{
			contextID = theContextID;

			beforeHandler = theBeforeHandler;
			afterHandler = theAfterHandler;
			beforeHandlerClassName = getClassName(beforeHandler);
			afterHandlerClassName = getClassName(afterHandler);

			//TOBIDEBUG
			//System.debug('TriggerManager<' + contextId + '>:' + Trigger.newList()[0].Id + ':' + ( ( ONB2__Invoice__c)Trigger.oldList()[0]).ONB2__Status__c );

			System.debug('$$$MUH$$$TriggerManager<' + contextId + '>isInsert:<' + Trigger.isInsert +'>isUpdate:' + Trigger.isUpdate);
			System.debug('$$$MUH$$$TriggerManager<' + contextId + '>:' + Trigger.newMap);


			//Init enabled trigger handler classes and keep trigger event name
			if (!isInitialized)
			{
				enabledBeforeHandlerClassName = beforeHandlerClassName;
				enabledAfterHandlerClassName = afterHandlerClassName;

				// Clear records of the invoced trigger handlers (used for debugging etc)
				beforeHandlerInvocations.clear();
				afterHandlerInvocations.clear();

				//Keep trigger event name
				if (Trigger.isInsert)
				{
					enabledTriggerEventName = INSERT_EVENT_NAME;
				}
				else if (Trigger.isUpdate)
				{
					enabledTriggerEventName = UPDATE_EVENT_NAME;
				}
				else if (Trigger.isDelete)
				{
					enabledTriggerEventName = DELETE_EVENT_NAME;
				}
				else if (Trigger.isUndelete)
				{
					enabledTriggerEventName = UNDELETE_EVENT_NAME;
				}

				isInitialized = true;
			}
		}

        /**
            Execute functionality in trigger handler classes if available
        */
		public void runHandlers()
		{
			if (!allTriggersDisabled)
			{
				//Call an enabled trigger handler class for 'onBefore' trigger action
				if (Trigger.isBefore && beforeHandler != null && (enabledBeforeHandlerClassName == beforeHandlerClassName || allTriggersEnabled))
				{
					if (Trigger.isInsert && (enabledTriggerEventName == INSERT_EVENT_NAME || allTriggersEnabled))
					{
						beforeHandlerInvocations.add(beforeHandlerClassName + '.onBeforeInsert');
						beforeHandler.onBeforeInsert(Trigger.new);
					}
					else if (Trigger.isUpdate && (enabledTriggerEventName == UPDATE_EVENT_NAME || allTriggersEnabled))
					{
						beforeHandlerInvocations.add(beforeHandlerClassName + '.onBeforeUpdate');
						beforeHandler.onBeforeUpdate(Trigger.new, Trigger.newMap, Trigger.old, Trigger.oldMap);
					}
					else if (Trigger.isDelete && (enabledTriggerEventName == DELETE_EVENT_NAME || allTriggersEnabled))
					{
						beforeHandlerInvocations.add(beforeHandlerClassName + '.onBeforeDelete');
						beforeHandler.onBeforeDelete(Trigger.old, Trigger.oldMap);
					}
				}
				//Call an enabled trigger handler class for 'onAfter' trigger action
				else if (Trigger.isAfter && afterHandler != null && (enabledAfterHandlerClassName == afterHandlerClassName || allTriggersEnabled))
				{
					if (Trigger.isInsert && (enabledTriggerEventName == INSERT_EVENT_NAME || allTriggersEnabled))
					{
						afterHandlerInvocations.add(afterHandlerClassName + '.onAfterInsert');
						afterHandler.onAfterInsert(Trigger.new, Trigger.newMap);
					}
					else if (Trigger.isUpdate && (enabledTriggerEventName == UPDATE_EVENT_NAME || allTriggersEnabled))
					{
						afterHandlerInvocations.add(afterHandlerClassName + '.onAfterUpdate');
						afterHandler.onAfterUpdate(Trigger.new, Trigger.newMap, Trigger.old, Trigger.oldMap);
					}
					else if (Trigger.isDelete && (enabledTriggerEventName == DELETE_EVENT_NAME || allTriggersEnabled))
					{
						afterHandlerInvocations.add(afterHandlerClassName + '.onAfterDelete');
						afterHandler.onAfterDelete(Trigger.old, Trigger.oldMap);
					}
					else if (Trigger.isUndelete && (enabledTriggerEventName == UNDELETE_EVENT_NAME || allTriggersEnabled))
					{
						afterHandlerInvocations.add(afterHandlerClassName + '.onAfterUndelete');
						afterHandler.onAfterUndelete(Trigger.new, Trigger.newMap);
					}
				}

			}
		}

        /**
            Helper method to get the class name for a given Object.
            Used as the key identifier of the trigger handler classes.
        */
		private String getClassName(Object theObject)
		{
			String className = null;
			if (theObject != null)
			{
				// An example of value: "ProjectTriggerHandler:[idToOldProjectMap=null, newProjectList=null]"
				String objectAsString = String.valueOf(theObject);
				className = objectAsString.substring(0, objectAsString.indexOf(':'));
			}
			return className;
		}
	}


    /**
        Interface definitions for the before trigger handlers
    */
	public interface BeforeTriggerHandler
	{
		void onBeforeInsert (List<sObject> newList);

		void onBeforeUpdate (List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap);

		void onBeforeDelete (List<sObject> oldList, Map<Id, sObject> oldMap);
	}

    /**
        Interface definitions for the after trigger handlers
    */
	public interface AfterTriggerHandler
	{
		void onAfterInsert (List<sObject> newList, Map<Id, sObject> newMap);

		void onAfterUpdate (List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap);

		void onAfterDelete (List<sObject> oldList, Map<Id, sObject> oldMap);

		void onAfterUndelete (List<sObject> newList, Map<Id, sObject> newMap);
	}

    /**
        An abstract adapter class that can be extended for convenience.
        By extending this class only the relevant handler methods need to be implemented.
    */
	public abstract class TriggerHandlerAdapter implements BeforeTriggerHandler, AfterTriggerHandler
	{
		public virtual void onBeforeInsert (List<sObject> newList) {}

		public virtual void onBeforeUpdate (List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap) {}

		public virtual void onBeforeDelete (List<sObject> oldList, Map<Id, sObject> oldMap) {}

		public virtual void onAfterInsert (List<sObject> newList, Map<Id, sObject> newMap) {}

		public virtual void onAfterUpdate (List<sObject> newList, Map<Id, sObject> newMap, List<sObject> oldList, Map<Id, sObject> oldMap) {}

		public virtual void onAfterDelete (List<sObject> oldList, Map<Id, sObject> oldMap) {}

		public virtual void onAfterUndelete (List<sObject> newList, Map<Id, sObject> newMap) {}
	}
}