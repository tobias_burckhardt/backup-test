public with sharing class test2 {
      // Controller for ExportData.page,
      // which expects to be handed the following POST variables:
      // @inputdata(string) data to export (i.e. in CSV format)
      // @filename(string) name of the file to export, e.g. 'AccountData' 

      public transient String input { public get; public set; }
      public transient String fileName { public get; public set; }

      public test2 () {
      }
      
      public test2 (ApexPages.StandardController stdController) {
            
      }
        
      public PageReference exportData() {
          return Page.Print;
          //return Page.DruckMich;
      }



    class NormalizedForecast  {
      public NormalizedForecast (Decimal year) {
          this.year = year;
      }
      public Decimal Year{ public get; public set; }
    }

    public List<NormalizedForecast> getData(){
     
           List<NormalizedForecast> cs = new List<NormalizedForecast>();
           for (Forecast__c c : [Select  Name, Year__c from Forecast__c])
           {       
               cs.add(new NormalizedForecast(c.Year__c));
           }
           return cs;
     }
       
      public Decimal muh { public get; public set; }

      public void exportContent(){
          Map<String,String> params = ApexPages.currentPage().getParameters();

          // We expect to be handed POST variables called 'inputdata' and 'filename'
          fileName = params.get('filename');
          if (fileName == null) fileName = 'Data';

          input = params.get('inputdata');
          if (input == null) input = 'No data provided.';
     }
}