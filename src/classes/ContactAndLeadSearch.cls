public with sharing class ContactAndLeadSearch {
	public ContactAndLeadSearch() {
		System.debug('Contact and lead search');
	}

	public static List<List<SObject>> searchContactsAndLeads(String name) {
		List<List<SObject>> contactSearchResultList = [
			FIND :name IN NAME FIELDS RETURNING Lead,Contact
		];

		System.debug(contactSearchResultList);
		return contactSearchResultList;
	}

}