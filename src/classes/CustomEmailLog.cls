/**
    <description>
     
    @author tobias
    @copyright PARX   
*/
public with sharing class CustomEmailLog
{
	private static List<String> logMessageList = new List<String>();

	public static void debug(String text) {
			logMessageList.add(text);
	}

	public static void submit() {
		Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
		message.toAddresses = new String[]{'tobias.burckhardt@googlemail.com'};
		message.optOutPolicy = 'FILTER';

		message.subject = 'Debug log';
		message.plainTextBody = logMessageList + '';

		if (logMessageList.size() == 0) {
			System.debug('no email sent. no messages available.');
			return;
		}

		Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
		Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);

		if (results[0].success) {
			System.debug('The email was sent successfully.');
		} else {
			System.debug('The email failed to send: ' + results[0].errors[0].message);
		}

	}
}