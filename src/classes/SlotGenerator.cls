/**
    <description>
     
    @author tobias
    @copyright PARX   
*/
public with sharing class SlotGenerator
{

	public static void generateSlots() {
		List<Slot__c> SobjLst = [select id from Slot__c];
		delete SobjLst;

		Territory__c t = [ select id, name from Territory__c where name='EVM Koblenz' ];

		Id recordTypeId = Schema.SObjectType.Slot__c.getRecordTypeInfosByName()
				.get('EFB').getRecordTypeId();

		List<Slot__c> slotlist = new List<Slot__c>();
		Slot__c s = new Slot__c();
		s.OwnerId='0050Y000000dQr6QAE';
		s.Start__c=Datetime.valueOf('2018-04-30 15:00:00');
		s.End__c=Datetime.valueOf('2018-04-30 17:00:00');
		s.Territory__r=t;
		s.RecordTypeId=recordTypeId;
		slotlist.add(s);
		s = new Slot__c();
		s.OwnerId='0050Y000000dQr6QAE';
		s.Start__c=Datetime.valueOf('2018-04-30 17:00:00');
		s.End__c=Datetime.valueOf('2018-04-30 19:00:00');
		s.Territory__r=t;
		s.RecordTypeId=recordTypeId;
		slotlist.add(s);
		s = new Slot__c();

		s.OwnerId='0050Y000000dQr6QAE';
		s.Start__c=Datetime.valueOf('2018-04-30 19:00:00');
		s.End__c=Datetime.valueOf('2018-04-30 21:00:00');
		s.Territory__r=t;
		s.RecordTypeId=recordTypeId;
		slotlist.add(s);
		s = new Slot__c();


		s.OwnerId='0050Y000000dQr6QAE';
		s.Start__c=Datetime.valueOf('2018-05-01 11:00:00');
		s.End__c=Datetime.valueOf('2018-05-01 13:00:00');
		s.Territory__r=t;
		s.RecordTypeId=recordTypeId;
		slotlist.add(s);
		s = new Slot__c();


		s.OwnerId='0050Y000000dQr6QAE';
		s.Start__c=Datetime.valueOf('2018-05-01 13:00:00');
		s.End__c=Datetime.valueOf('2018-05-01 15:00:00');
		s.Territory__r=t;
		s.RecordTypeId=recordTypeId;
		slotlist.add(s);

		s = new Slot__c();
		s.OwnerId='0050Y000000dQr6QAE';
		s.Start__c=Datetime.valueOf('2018-05-01 15:00:00');
		s.End__c=Datetime.valueOf('2018-05-01 17:00:00');
		s.Territory__r=t;
		s.RecordTypeId=recordTypeId;
		slotlist.add(s);

		s = new Slot__c();
		s.OwnerId='0050Y000000dQr6QAE';
		s.Start__c=Datetime.valueOf('2018-05-01 17:00:00');
		s.End__c=Datetime.valueOf('2018-05-01 19:00:00');
		s.Territory__r=t;
		s.RecordTypeId=recordTypeId;
		slotlist.add(s);

		s = new Slot__c();
		s.OwnerId='0050Y000000dQr6QAE';
		s.Start__c=Datetime.valueOf('2018-05-01 19:00:00');
		s.End__c=Datetime.valueOf('2018-05-01 21:00:00');
		s.Territory__r=t;
		s.RecordTypeId=recordTypeId;
		slotlist.add(s);

		s = new Slot__c();
		s.OwnerId='0050Y000000dQr6QAE';
		s.Start__c=Datetime.valueOf('2018-05-02 11:00:00');
		s.End__c=Datetime.valueOf('2018-05-02 13:00:00');
		s.Territory__r=t;
		s.RecordTypeId=recordTypeId;
		slotlist.add(s);

		s = new Slot__c();
		s.OwnerId='0050Y000000dQr6QAE';
		s.Start__c=Datetime.valueOf('2018-05-02 13:00:00');
		s.End__c=Datetime.valueOf('2018-05-02 15:00:00');
		s.Territory__r=t;
		s.RecordTypeId=recordTypeId;
		slotlist.add(s);

		s = new Slot__c();
		s.OwnerId='0050Y000000dQr6QAE';
		s.Start__c=Datetime.valueOf('2018-05-03 15:00:00');
		s.End__c=Datetime.valueOf('2018-05-03 17:00:00');
		s.Territory__r=t;
		s.RecordTypeId=recordTypeId;
		slotlist.add(s);

		s = new Slot__c();
		s.OwnerId='0050Y000000dQr6QAE';
		s.Start__c=Date.valueOf('2018-05-03 17:00:00');
		s.End__c=Date.valueOf('2018-05-03 19:00:00');
		s.Territory__r=t;
		s.RecordTypeId=recordTypeId;
		slotlist.add(s);

		s = new Slot__c();

		s.OwnerId='0050Y000000dQr6QAE';
		s.Start__c=Date.valueOf('2018-05-03 19:00:00');
		s.End__c=Date.valueOf('2018-05-03 21:00:00');
		s.Territory__r=t;
		s.RecordTypeId=recordTypeId;
		slotlist.add(s);
		insert slotlist;

	}

}