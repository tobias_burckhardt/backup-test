@isTest
private class Test_SOSLSOQL {

	@isTest static void test_soql() {
		List<Campsite__c> createCampsiteData = createCampsiteData( 100, 'campsite1' );
		createCampsiteReservationData( 100, 'campsiteReservation1' );

		Test.startTest();

		List<Contact> campRes = [ Select Id from Contact ];
		System.debug(campRes.size() + ':' + campRes);

		Test.stopTest();	
	}

	@isTest static void test_sosl() {
		List<Campsite__c> campsiteData = createCampsiteData( 100, 'campsite1' );
		createCampsiteReservationData( 100, 'campsiteReservation2' );

		Test.startTest();

		List<List<Contact>> campRes = [ FIND 'A*' IN ALL FIELDS RETURNING Contact(name) ];
		System.debug(campRes.size() + ':' + campRes);

		Test.stopTest();	
	}

	private static List<Campsite__c> createCampsiteData(Integer amount, String namePrefix) {
		List<Campsite__c> campsiteList = new List<Campsite__c>();
		for ( Integer i = 0; i < amount; i++) {
			campsiteList.add( new Campsite__c( name = namePrefix ) );
		}
		insert campsiteList;
		return campsiteList;
	}


	private static List<Campsite_Reservation__c> createCampsiteReservationData(Integer amount, String prefix) {
		List<Campsite__c> campsiteList = createCampsiteData( 100, 'campsite' + '_' + prefix );

		User user = [ Select Id from User][0];
		List<Campsite_Reservation__c> campsiteReservationList = new List<Campsite_Reservation__c>();

		for ( Integer i = 0; i < amount; i++) {
			campsiteReservationList.add(new Campsite_Reservation__c( Campsite__c = campsiteList[i].Id, User__c = user.Id ) );
		}
		insert campsiteReservationList;
		return campsiteReservationList;
	}
}