<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>Waypoint__c</defaultLandingTab>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Trailblazer</label>
    <tabs>standard-Chatter</tabs>
    <tabs>standard-File</tabs>
    <tabs>Waypoint__c</tabs>
    <tabs>standard-report</tabs>
    <tabs>standard-Dashboard</tabs>
    <tabs>Battle_Station__c</tabs>
    <tabs>Resource__c</tabs>
    <tabs>Supply__c</tabs>
    <tabs>Position__c</tabs>
    <tabs>Retention__c</tabs>
    <tabs>Products__x</tabs>
    <tabs>Camping_Item__c</tabs>
    <tabs>ContentRef__c</tabs>
    <tabs>RegularAvailability__c</tabs>
    <tabs>Territory__c</tabs>
    <tabs>Slot__c</tabs>
    <tabs>Project__c</tabs>
    <tabs>Forecast__c</tabs>
    <tabs>Qote__c</tabs>
</CustomApplication>
