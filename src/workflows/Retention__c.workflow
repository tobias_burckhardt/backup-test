<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Your_expiring_retentions</fullName>
        <description>Your expiring retentions</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>testemail/TestA</template>
    </alerts>
    <rules>
        <fullName>ret</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Retention__c.EndDate__c</field>
            <operation>greaterThan</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Your_expiring_retentions</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Retention__c.EndDate__c</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
