/*
	Handler for SubscribedArea related triggers.

 	@author tbu
 	@copyright PARX
*/
@isTest
public class Test_SubscribedAreaTriggerHandler
{

    /*
        Test for a working duplicate record prevention within subscription_area__c object by creating two unique
        SObjects more than once
     */
	@isTest static void testDuplicateRecordPreventionTrigger()
	{
		Integer entries = 200;
		Boolean bulkInsertErrorForAlreadyExistingRecordOccurred = false;
		Boolean bulkInsert401ErrorForAlreadyExistingRecordOccurred = false;
		Boolean singleInsertErrorForAlreadyExistingRecordOccurred = false;

		List<Subscribed_Area__c> bulkInsertSubscribedAreaRecordList = new List<Subscribed_Area__c>();
		List<Subscribed_Area__c> bulkInsertSubscribedAreaDuplicateRecordList = new List<Subscribed_Area__c>();
		List<Subscribed_Area__c> bulkInsert401SubscribedAreaDuplicateRecordList  = new List<Subscribed_Area__c>();

		List<Area__c> areasInserted = createAreas('CAN', entries);
		List<Lead_Subscription__c> leadSubscriptionsInserted = createLeadSubscriptions('LEAD', entries);


		//prepare testdata
		List<List<Id>> subscribedAreaInsertDataTuple = new List<List<Id>>();
		for (Integer i = 0; i < entries; i++)
		{
			subscribedAreaInsertDataTuple.add(new List<Id> {areasInserted[i].Id, leadSubscriptionsInserted[i].Id});
		}

		for (Integer i = 0; i < 21; i++)
		{
			bulkInsertSubscribedAreaDuplicateRecordList.add(createSubscribedArea(subscribedAreaInsertDataTuple.get(1)));
		}

		for (Integer i = 0; i < 401; i++)
		{
			bulkInsert401SubscribedAreaDuplicateRecordList.add(createSubscribedArea(subscribedAreaInsertDataTuple.get(1)));
		}

		for (Integer i = 1; i < entries; i++)
		{
			bulkInsertSubscribedAreaRecordList.add(createSubscribedArea(subscribedAreaInsertDataTuple.get(i)));
		}


		Test.startTest();

		//insert 1 record twice
		insert createSubscribedArea(subscribedAreaInsertDataTuple.get( 0 ));

		try
		{

			//should fail because subscribed area already contains this record
			insert createSubscribedArea(subscribedAreaInsertDataTuple.get( 0 ));

		} catch (Exception e)
		{
			singleInsertErrorForAlreadyExistingRecordOccurred = e.getMessage().contains(Label.SubscribedAreaDuplicationPrevention);
		}
		System.assert(singleInsertErrorForAlreadyExistingRecordOccurred, 'duplicate record prevention failed');


		//bulk test - does it work for multiple records?
		try
		{
			insert bulkInsertSubscribedAreaDuplicateRecordList;
		} catch (Exception e)
		{
			bulkInsertErrorForAlreadyExistingRecordOccurred = e.getMessage().contains(Label.SubscribedAreaDuplicationPrevention);
		}
		System.assert(bulkInsertErrorForAlreadyExistingRecordOccurred, 'duplicate record prevention failed');

		//bulk test - does it work for multiple records 200+?
		try
		{
			insert bulkInsert401SubscribedAreaDuplicateRecordList;
		}
		catch (Exception e)
		{
			bulkInsert401ErrorForAlreadyExistingRecordOccurred = e.getMessage().contains(Label.SubscribedAreaDuplicationPrevention);
		}
		System.assert(bulkInsert401ErrorForAlreadyExistingRecordOccurred, 'duplicate record prevention failed');

		//a working bulk insert
		insert bulkInsertSubscribedAreaRecordList;

		//test for required records
		List<Subscribed_Area__c> actualSubscribedAreaList = [Select Id, Area__c, Lead_Subscription__c from Subscribed_Area__c];
		System.assertEquals(subscribedAreaInsertDataTuple.size(), actualSubscribedAreaList.size(), 'size of subscribed_area__c records in sf database not as expected' );

		Test.stopTest();
	}


	/**
		Creates a new SubscribedArea__c Object for given Area__c and Lead_Subscription__c ids
	 */
	static Subscribed_Area__c createSubscribedArea(List<Id> idList)
	{

		Subscribed_Area__c subscribed_area = new Subscribed_Area__c();
		subscribed_area.Area__c = idList.get(0);
		subscribed_area.Lead_Subscription__c = idList.get(1);

		return subscribed_area;
	}

    /**
        Create <amount> new Area__c records with given prefix for Name and AreaKey__c
     */
	static List<Area__c> createAreas(String keyPrefix, Integer amount)
	{
		List<Area__c> areas = new List<Area__c>();
		for (Integer i = 0; i < amount; i++)
		{
			areas.add(new Area__c(Name = keyPrefix + '_' + i, AreaKey__c = keyPrefix + '_' + i));
		}
		insert areas;
		return areas;
	}

    /**
        Create <amount> new Lead_Subscription__c records with given prefix for description__c
     */
	static List<Lead_Subscription__c> createLeadSubscriptions(String descriptionPrefix, Integer amount)
	{
		Lead_Subscription__c[] leadSubscriptions = new List<Lead_Subscription__c>();
		for (Integer i = 0; i < amount; i++)
		{
			leadSubscriptions.add(new Lead_Subscription__c(description__c = descriptionPrefix + '_' + i));
		}
		insert leadSubscriptions;
		return leadSubscriptions;
	}

}