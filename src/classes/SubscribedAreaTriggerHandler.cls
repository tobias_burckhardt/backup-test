/**
	Handler for SubscribedArea related triggers.

 	@author tbu
 	@copyright PARX
*/
public with sharing class SubscribedAreaTriggerHandler
{

	/**
		Mark all duplicate trigger records
    */
	public class DuplicateRecordsPreventer implements Triggers.Handler
	{

		/**
			Implementation of handle method. We check for already existing uniqueKeys(Area__c,LeadSubscription__c)
		 */
		public void handle()
		{
			System.debug(
					System.LoggingLevel.FINEST, 'SubscribedAreaTriggerHandler:DuplicateRecordsPreventer -> enter handle');

			Map<String, Subscribed_Area__c> triggerRecordsUniqueKeyMap = new Map<String, Subscribed_Area__c>();
			List<String> areaIdList = new List<String>();
			List<String> leadSubscriptionIdList = new List<String>();
			Set<Subscribed_Area__c> subscribedAreaErrorRecordList = new Set<Subscribed_Area__c>();

			for (Subscribed_Area__c subscribedArea : (List<Subscribed_Area__c>) Trigger.new)
			{
				String uniqueKey = locateUniqueKeyForSubscribedArea(subscribedArea);

				//already part of insert records
				if (triggerRecordsUniqueKeyMap.containsKey (uniqueKey))
				{
					subscribedAreaErrorRecordList.add(subscribedArea);

				}
				else
				{
					triggerRecordsUniqueKeyMap.put(uniqueKey, subscribedArea);
				}

				areaIdList.add(subscribedArea.Area__c);
				leadSubscriptionIdList.add(subscribedArea.Lead_Subscription__c);

			}

			//find potential already existing records
			List<Subscribed_Area__c> alreadyExistingSubscribedAreaList =
			[
					Select Id, Area__c, Lead_Subscription__c
					from Subscribed_Area__c
					where Area__c in :areaIdList and Lead_Subscription__c in :leadSubscriptionIdList
			];

			//search resultset for duplicates
			for (Subscribed_Area__c existingSubscribedArea : alreadyExistingSubscribedAreaList)
			{
				String uniqueKey = locateUniqueKeyForSubscribedArea(existingSubscribedArea);
				if (triggerRecordsUniqueKeyMap.containsKey(uniqueKey))
				{
					subscribedAreaErrorRecordList.add(triggerRecordsUniqueKeyMap.get(uniqueKey));
				}
			}

			//mark all found records as error
			for (Subscribed_Area__c subscribedAreaErrorRecord : subscribedAreaErrorRecordList)
			{
				subscribedAreaErrorRecord.addError(Label.SubscribedAreaDuplicationPrevention);
			}
			System.debug('records marked as error:' + subscribedAreaErrorRecordList.size());

		}

		/**
			Identifies unique key constraint for SubscribedArea
		 */
		private String locateUniqueKeyForSubscribedArea(Subscribed_Area__c subscribedArea)
		{
			return subscribedArea.Area__c + ':' + subscribedArea.Lead_Subscription__c;
		}

	}
}